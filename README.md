This project is a misguided hack that ideally would be removed by a proper way of providing configuration to a python library, pull requests to projects using this are welcome.

In meantime, you can clone this repo, modify [global_config.py](global_config.py) and install this as a python package.

You can use `reinstall.sh` script to install/reinstall it.

So:

```
git clone https://codeberg.org/matkoniecz/osm_handling_config.git
cd osm_handling_config/osm_handling_config
codium global_config.py # or whatever other editor you use
cd ..
bash reinstall.sh
```