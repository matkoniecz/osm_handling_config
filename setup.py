import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="osm_handling_config",
    version="0.0.1",
    author="not admitting to this one",
    author_email="pleaseignore@example.com",
    description="A horrific idea",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://codeberg.org/matkoniecz/osm_handling_config",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # for dependencies see https://python-packaging.readthedocs.io/en/latest/dependencies.html
) 
